import csv, pickle, os, sys
import numpy as np
from sklearn.metrics import roc_auc_score, f1_score, precision_recall_fscore_support, accuracy_score, precision_recall_curve, average_precision_score
import argparse
import math
from sklearn import metrics
CLASSES = 30 # indicate the number of diseases

def reoptimization(args):
    # 'rbf_sim.pickle': saving the disease embeddings got from knowledge graph training
    with open('rbf_sim.pickle', "rb") as fp:
        rbf_sim_matrix = pickle.load(fp) # shape: embedindex * embedindex
    print('shape of rbf_sim_matrix:'+str(len(rbf_sim_matrix)))
    entities_indexes = {}
    relations_indexes = {}
    for split in ['train', 'valid', 'test']:
        with open(os.path.join(args.data_dir,split+"_dict.pickle"), "rb") as fp:
            dictionary = pickle.load(fp)
            for k, v in dictionary[0].items():
                if k not in entities_indexes:
                    entities_indexes[k] = v # entities_indexes[cui] = embedindex
            for k, v in dictionary[1].items():
                if k not in relations_indexes:
                    relations_indexes[k] = v # relations_indexes[cui] = embedindex
    
    print('-----cui 2 embedindex')
    print(entities_indexes)
    file = open('/home/qlin/MIMIC-III/disease30.csv',"r") 
    csv_reader = file.readlines()
    cui2col = {}
    cui_lis = []

    for line in csv_reader:
        tokens = line.split(',')
        CUI = tokens[2].strip('\r\n').split('/')
        colindex = tokens[0]
        if len(CUI) > 1:
            for i in range(len(CUI)):
                if i == 0:
                    cui2col[colindex] = [CUI[i]]
                else:
                    cui2col[colindex].append(CUI[i]) # cui2col[colindex] = cui
        else:
            cui2col[colindex] = CUI
    file.close()

    S_dis = []
    for i in range(CLASSES):
        lst = []
        c = 0
        print('i:'+str(i))
        cutoff = 0.46
    	for j in range(CLASSES):
            try:
                iCUI = cui2col[str(i)]
                jCUI = cui2col[str(j)]

                if len(iCUI) == 1 and len(jCUI) == 1:
                    if iCUI[0] in entities_indexes and jCUI[0] in entities_indexes:
                        sim = rbf_sim_matrix[iCUI[0]][jCUI[0]]
                        total = [1] # just to make length of total to be 1
                elif len(iCUI) == 1 and len(jCUI) > 1:
                    total = []
                    for k in jCUI:
                        if iCUI[0] in entities_indexes and k in entities_indexes:
                            total.append(rbf_sim_matrix[iCUI[0]][k])
                    sim = max(total)

                elif len(iCUI) > 1 and len(jCUI) == 1:
                    total = []
                    for k in iCUI:
                        if k in entities_indexes and jCUI[0] in entities_indexes:
                            total.append(rbf_sim_matrix[k][jCUI[0]])
                    sim  = max(total)
                else:
                    total = []
                    for k in iCUI:
                        for v in jCUI:
                            if k in entities_indexes and v in entities_indexes:
                                total.append(rbf_sim_matrix[k][v])
                    sim = max(total)
                if sim > cutoff:
                    lst.append(sim)
                else:
                    lst.append(0.0)
            except ValueError:
                if i == j:
                    lst.append(1.0)
                else:
                    lst.append(0.0)

        newlst = [t for t, n in enumerate(lst) if n > 0]
        print(str(i)+','+str(len(newlst)) +','+str(newlst))
        S_dis.append(lst)
    S_dis = np.asarray(S_dis)
    np.where(S_dis > 0.999999, 1, S_dis)
    S_dis[8, 8] = 1
    S_dis[13, 13] = 1
    S_dis[21, 21] = 1
    S_dis[25, 25] = 1 
    np.save(open('/home/qlin/MIMIC-III/similarity.npy','w'), S_dis)
    print('S_dis:'+str(S_dis))
     
    threshold = 0.75 
    predictions = np.load('../predpatient.npy') #ground truth
    print('nan:'+str(np.isnan(np.min(predictions))))
    print(np.argwhere(np.isnan(predictions)))
    print(predictions.shape)
    labels = np.load('../testpatient.npy') #predictions
    labels = np.where(labels < 1, labels, 1)
    print('shape of pred and labels:'+str(predictions.shape)+','+str(labels.shape))
    print(predictions[2,:50])
    print(labels[2,:50])    

    rare = [18, 20, 22,23,24,25,26,27,28,29]
    label = labels[:, rare]
    prediction = predictions[:, rare]
    for mode in ['micro', 'macro']:
        try:
            print(mode+" auroc: "+str(roc_auc_score(labels, predictions, average = mode)))
            print(mode+" auroc for rare: "+str(roc_auc_score(label, prediction, average = mode)))
            print(mode+" precision, recall, fscore: "+str(precision_recall_fscore_support(y_true=labels, y_pred=(predictions>=0.5).astype(int), average=mode)))
            for k in range(CLASSES):
                y_pred=(predictions[:,k]>=threshold).astype(int)
            	#y_pred=P_double_hat[:,k]
            	y_true=labels[:,k]
            	hamming_loss = metrics.hamming_loss(y_true, y_pred)
	    	print('label '+str(k)+' precision, recall, fscore, auc'+str(k)+':'+str(precision_recall_fscore_support(y_true=labels[:,k], y_pred=(predictions[:,k]>=threshold).astype(int), average=mode))+','+str(roc_auc_score(y_true=labels[:,k], y_score=predictions[:,k]))+','+str(hamming_loss))
        except:
            print("Something went wrong")

    print("accuracy: ",accuracy_score(y_true=labels, y_pred=(predictions>=0.7).astype(int)))
    print("total hamming_loss: ",metrics.hamming_loss(y_true=labels, y_pred=(predictions>=0.7).astype(int)))
    

    best_P_hat = predictions
    best_precision = 0.0

    for eps in [0.9]: # you can try different eps values, e.g., [0.1, 0.25, 0.5, 0.75, 0.9]
        P_double_hat = np.random.uniform(0,1,size=best_P_hat.shape)
        P_double_hat_new = np.zeros(P_double_hat.shape)
        S_dis_tiled_samples = np.tile(S_dis[None, : ,:], (best_P_hat.shape[0],1,1)) # T * D * D
        S_dis_tiled_samples_sum = np.sum(S_dis_tiled_samples, axis=1).astype(np.float32) # T * D

        for it in range(50):
            if it < 1:
                tiled_P_hat_dis = np.tile(best_P_hat[:,:,None], (1,1,CLASSES)) # T * D * D
            else:
                tiled_P_hat_dis = np.tile(P_double_hat_new[:,:,None], (1,1,CLASSES)) # T * D * D
            #print("shape of tiled_P_hat_dis and S_dis_tiled_samples:"+str(tiled_P_hat_dis.shape)+','+str(S_dis_tiled_samples.shape))
            tiled_P_hat_dis = tiled_P_hat_dis * S_dis_tiled_samples
	    
            P_double_hat_new = np.nan_to_num((np.sum(tiled_P_hat_dis, axis = 1) * (1-eps))/ S_dis_tiled_samples_sum + (eps * best_P_hat)) # T * D
            P_double_hat = np.nan_to_num(P_double_hat_new.copy())
            diff = np.tile(P_double_hat_new[:,None,:],[1,CLASSES,1]) - np.tile(P_double_hat_new[:,:,None],[1,1,1]) # T * D * D
            bias = (1 - eps) * np.sum(np.square(diff) * S_dis_tiled_samples) + eps * np.sum(S_dis_tiled_samples_sum * np.square(P_double_hat_new - predictions))
            print('bias of '+str(it)+':'+str(bias))
        print("Epsilon: "+str(eps))
        #np.save(open('../predpatients_counter_kl.npy','w'), P_double_hat_new)
        #np.save(open('../predpatients_nokn_counter_kl.npy','w'), P_double_hat_new)  #predpatients_cocoa_kl.npy      
        hat_rare = P_double_hat[:, rare]
        for mode in ['micro', 'macro']:
            try:
            	print(mode+" AUROC: "+str(roc_auc_score(y_true=labels, y_score=P_double_hat, average = mode))) 
            	print(mode+" auroc for rare: "+str(roc_auc_score(label, hat_rare, average = mode)))
                precision, recall, f_score, _ = precision_recall_fscore_support(y_true=labels, y_pred=(P_double_hat>=threshold).astype(int), average=mode)
            	print(mode+" precision, recall, fscore: "+str((precision, recall, f_score)))
            	for k in range(CLASSES):
                    y_pred=(P_double_hat[:,k]>=threshold).astype(int)
                    #y_pred=P_double_hat[:,k]
                    y_true=labels[:,k]
                    hamming_loss = metrics.hamming_loss(y_true, y_pred)
                    #ranking_loss = metrics.label_ranking_loss(y_true, y_pred)
                    precision, recall, f_score, _ = precision_recall_fscore_support(y_true=labels[:,k], y_pred=(P_double_hat[:,k]>=0.5).astype(int), average=mode)
                    print('label '+str(k)+' precision, recall, f_score, auc, hamming loss:'+str((precision, recall, f_score))+','+str(roc_auc_score(y_true=labels[:,k], y_score=P_double_hat[:,k]))+','+str(hamming_loss))
            except:
                print("Something went wrong")	
        #print("accuracy: ",accuracy_score(y_true=labels, y_pred=(P_double_hat>=0.7).astype(int)))
        #print("total hamming_loss: ",metrics.hamming_loss(y_true=labels, y_pred=(P_double_hat>=0.7).astype(int)))
        if best_precision < precision:
                  best_precision = precision
                  best_P_double_hat = P_double_hat

    np.save(open('../predpatient.npy','w'), P_double_hat)

    for j in [3,19,200]:
        for i in range(CLASSES):    
            print(round(P[j][i], 2), round(best_P_double_hat[j][i], 2), round(labels[j][i], 0))
        print("=====================================================")

    correct_preds = ((best_P_double_hat>=threshold).astype(int) == labels).astype(int)
    indices_P_double_hat = []
    for i in range(len(correct_preds)):
        if correct_preds[i].all() == 1:
            indices_P_double_hat.append(i)
    
    
    correct_preds = ((P>=threshold).astype(int) == labels).astype(int)
    indices = []
    for i in range(len(correct_preds)):
        if correct_preds[i].all() == 1:
            indices.append(i)

    extra_preds = list(set(indices_P_double_hat)-set(indices))


def main():
    parser = argparse.ArgumentParser(description='Process input file locations')
    parser.add_argument('--data_dir', default='/home/qlin/MIMIC-III/knowgraph/data')#'_semmed', help='dir where mimic3-benchmarks dataset is located')
    #parser.add_argument('--sim_score_file', default='zerohops_rbf_sim.pickle', help='file where similarity scores are stored')
    args = parser.parse_args()

    reoptimization(args)

if __name__ == '__main__':
    main()
