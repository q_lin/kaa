Multi-Disease Predictive Analytics: A Clinical Knowledge-Aware Approach

This repository explains how to apply KAA model proposed in the paper "Multi-Disease Predictive Analytics: A Clinical Knowledge-Aware Approach", given the embedded knowledge graph and prediction probabilities obtained from multi-label data driven model. The 30 diseases we used are saved in disease30.csv.

You can run the KAA by python KAA.py.
